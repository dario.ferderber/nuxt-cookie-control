export default {
  barTitle: 'Cookies',
  barDescription: 'Utilizamos cookies propias y de terceros para poder mostrarle un sitio web y comprender cómo lo utiliza, con el fin de mejorar los servicios que ofrecemos. Si continúas navegando, consideramos que aceptas el uso de cookies.',
  acceptAll: 'Aceptar todo',
  declineAll: 'Borrar todo',
  controlCookies: 'Gestionar cookies',
  unsaved: 'Tienes configuraciones no guardadas',
  close: 'Cerrar',
  save: 'Guardar',
  necessary: 'Cookies necesarias',
  optional: 'Cookies opcionales',
}