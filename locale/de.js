export default {
  barTitle: 'Cookies',
  barDescription: 'Wir verwenden unsere eigenen Cookies und Cookies von Drittanbietern, damit wir Ihnen eine Website zeigen und verstehen können, wie Sie diese verwenden, um die von uns angebotenen Dienstleistungen zu verbessern. Wenn Sie weiter surfen, gehen wir davon aus, dass Sie die Cookies akzeptiert haben.',
  acceptAll: 'Akzeptiere alle',
  declineAll: 'Alles löschen',
  controlCookies: 'Cookies verwalten',
  unsaved: 'Sie haben nicht gespeicherte Einstellungen',
  close: 'Close',
  save: 'Save',
  necessary: 'Notwendige cookies',
  optional: 'Optionale cookies',
}