export default {
  barTitle: 'Kolačići',
  barDescription: 'Koristimo vlastite kolačiće i kolačiće treće strane kako bismo Vam mogli prikazati web stranicu i razumijeti kako je koristite, s pogledom na poboljšanje usluga koje nudimo. Ako nastavite s pregledavanjem, smatramo da prihvaćate upotrebu kolačića.',
  acceptAll: 'Dozvoli sve',
  declineAll: 'Obriši sve',
  controlCookies: 'Upravljaj kolačićima',
  unsaved: 'Imate nespremljenih postavki',
  close: 'Zatvori',
  save: 'Spremi',
  necessary: 'Obavezni kolačići',
  optional: 'Neobavezni kolačići',
}